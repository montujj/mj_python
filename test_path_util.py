import unittest
from importlib import reload

import path_util as path_util
reload(path_util)


class TestFilePathDetection(unittest.TestCase):

    def setUp(self):
        self.is_path = path_util.is_path_candidate

    def test_valid_unix_file_path(self):
        self.assertTrue(self.is_path('/path/to/file.txt'))

    def test_valid_windows_file_path(self):
        self.assertTrue(self.is_path('C:\\path\\to\\file.txt'))

    def test_invalid_file_path(self):
        self.assertFalse(self.is_path('This is not a file path'))

    def test_empty_string(self):
        self.assertFalse(self.is_path(''))

    def test_no_path(self):
        self.assertFalse(self.is_path('filename.txt'))

    def test_only_separator(self):
        self.assertTrue(self.is_path('/'))


class TestNormalizePathSeparators(unittest.TestCase):

    def setUp(self):
        self.normalize = path_util.normalize_path_separators

    def test_normalize_seperator_with_forward_slashes(self):
        original_path = "C:/data/files/temp"
        expected_path = "C:\\data\\files\\temp"
        self.assertEqual(self.normalize(original_path), expected_path)

    def test_normalize_seperator_with_backslashes(self):
        original_path = "C:\\data\\files\\temp"
        expected_path = "C:\\data\\files\\temp"
        self.assertEqual(self.normalize(original_path), expected_path)

    def test_normalize_seperator_with_mixed_separators(self):
        original_path = "C:/data\\files/temp"
        expected_path = "C:\\data\\files\\temp"
        self.assertEqual(self.normalize(original_path), expected_path)

    def test_normalize_seperator_with_empty_string(self):
        original_path = ""
        expected_path = ""
        self.assertEqual(self.normalize(original_path), expected_path)


if __name__ == '__main__':
    # unittest.main()
    testloader1 = unittest.TestLoader().loadTestsFromTestCase(TestFilePathDetection)
    unittest.TextTestRunner(verbosity=2).run(testloader1)

    testloader2 = unittest.TestLoader().loadTestsFromTestCase(TestNormalizePathSeparators)
    unittest.TextTestRunner(verbosity=2).run(testloader2)
