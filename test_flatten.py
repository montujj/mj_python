import unittest
import flatten as flatten


class TestFlatten(unittest.TestCase):

    def test_flatten_invalid_input(self):
        """Test case with invalid input"""
        invalid_input = "invalid input"
        with self.assertRaises(TypeError):
            flatten.flatten(invalid_input)

    def test_flatten(self):
        """Test case with a nested array"""
        nested_array = [[1, 2, [3]], 4]
        expected_output = [1, 2, 3, 4]
        output = flatten.flatten(nested_array)
        self.assertEqual(output, expected_output)

    def test_flatten_nested_array_with_multiple_levels(self):
        """Test case with a nested array containing multiple levels"""
        nested_array = [[61, 25, [36]], [5, 6, 7, [9, [15]]]]
        expected_output = [61, 25, 36, 5, 6, 7, 9, 15]
        output = flatten.flatten(nested_array)
        self.assertEqual(output, expected_output)

    def test_flatten_empty_nested_array(self):
        """Test case with an empty nested array"""
        nested_array = []
        expected_output = []
        output = flatten.flatten(nested_array)
        self.assertEqual(output, expected_output)

    def test_flatten_single_element_nested_array(self):
        """Test case with a nested array containing a single element"""
        nested_array = [71]
        expected_output = [71]
        output = flatten.flatten(nested_array)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main(exit=False, verbosity=2)
