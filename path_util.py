import os

def is_path_candidate(arg):
    """
    Determine if a given string looks like a file path based on its format.

    This function checks if the provided argument is a string and contains
    path separators ('/' or '\'). It does not verify the actual existence
    of the file.

    Args:
        arg (str): The string to be checked.

    Returns:
        bool: True if the argument looks like a file path, False otherwise.

    Examples:
        print(is_path_candidate("path/to/file.txt"))  # True
        print(is_path_candidate("just_a_string"))    # False
        print(is_path_candidate("\\root\\file.txt"))   # True
    """
    if not isinstance(arg, str):
        return False

    if os.path.sep in arg or '/' in arg or '\\' in arg:
        return True

    return False


def normalize_path_separators(file_path):
    """
    Normalize the path separators in the given path.

    This function replaces forward slashes '/' and backslashes '\\' in the input
    path with the appropriate path separator for the operating system.

    Args:
        file_path (str): The input path.

    Returns:
        str: The path with normalized separators.

    Example:
        file_path = "C:/data\\files/temp"
        normalized_path = normalize_path_separators(file_path)
        print(normalized_path)  # Output: "C:\\data\\files\\temp"
    """

    # Replace '/' and '\\' with the appropriate path separator
    # file_path = file_path.replace('\\', os.path.sep).replace('/', os.path.sep)
    chars = [char if char not in '\\/' else os.path.sep for char in file_path]
    normalized_path = ''.join(chars)

    return normalized_path