import unittest
import temperature_tracker


class TestTemperatureTracker(unittest.TestCase):

    def setUp(self):
        self.temperature_list = [0, 1, 2, 3, 4, 110]
        self.tracker = temperature_tracker.TemperatureTracker()
        [self.tracker.insert_item(temp) for temp in self.temperature_list]

    def test_insert_item(self):
        temp = 80
        self.tracker.insert_item(temp)
        self.assertIn(temp, range(0, 111))
        print("inserting temperature {} >> {}".format(temp, 
                                                      self.temperature_list))

    def test_get_min(self):
        self.assertEqual(self.tracker.get_min(), 0)
        print("lowest temperature in {} is {}".format(self.temperature_list, 
                                                      self.tracker.get_min()))

    def test_get_max(self):
        self.assertEqual(self.tracker.get_max(), 110)
        print("highest temperature in {} is {}".format(self.temperature_list, 
                                                       self.tracker.get_max()))

    def test_get_mean(self):
        self.assertEqual(self.tracker.get_mean(), 20.0)
        print("average temperature in {} is {}".format(self.temperature_list,
                                                       self.tracker.get_mean()))


if __name__ == '__main__':
    testloader = unittest.TestLoader().loadTestsFromTestCase(TestTemperatureTracker)
    unittest.TextTestRunner(verbosity=2).run(testloader)
