

class TemperatureTracker(object):
    """A class to track Temperature."""

    def __init__(self):
        """ create empty list to store all user input temperatures."""
        self.temperature = []

    def insert_item(self, temp):
        """add temperature to existing list.

        temperature will be recorded in Fahrenheit
        integer ranging from value 0-110

        Args:
            temp (int): current input temperature to record

        Raises:
            ValueError: If `temp` not in range from 1-110

        """
        if temp not in range(0, 111):
            raise ValueError('input value out of range 0-110')
        self.temperature.append(int(temp))

    def get_min(self):
        """get lowest recorded temperature we have seen so far.

        Returns:
        int: lowest temperature from all the stored values.

        """
        return min(self.temperature)

    def get_max(self):
        """get highest recorded temperature.

        Returns:
        int: highest temperature from all the stored values.

        """
        return max(self.temperature)

    def get_mean(self):
        """get average temperature from all the stored values.

        Returns:
        float: median value of all temperature.

        """
        total = float(sum(self.temperature))
        count = len(self.temperature)
        median = total/count
        return median
