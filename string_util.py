import re


def count_characters(user_input):
    """
    Count the occurrence of each character in the given string and create 
    a dictionary.

    Args:
        user_input (str): The input string.

    Returns:
        dict: A dictionary where each character is a key and the value is the 
        count of its occurrences.

    """
    character_count = {}
    # Count the occurrence of each character in the string
    for char in user_input:
        # Exclude empty spaces
        if char.strip() != "":
            if char in character_count:
                # Increment the count if the character is repeating
                character_count[char] += 1
            else:
                character_count[char] = 1

    return character_count


def find_maximum_occurrence_character(user_input, verbose=1):
    """
    Find the character with the maximum occurrence in the given input string.

    Args:
        user_input (str): The input string.

    Returns:
        str: The character with the maximum occurrence.

    """

    count_result = count_characters(user_input)
    count_list = count_result.values()
    result = None
    for key, value in count_result.items():
        if count_result[key] == max(count_list):
            result = key
            if verbose:
                print("'{}' appears {} times in '{}'".format(result,
                                                             count_result[key],
                                                             user_input))

    return result


def find_first_unique_character(user_input):
    """Find first unique character from a string.

    Args:
        user_input (str): The input string.

    Returns:
        str: The first unique character, or None if not found.

    Examples:
        >>> find_first_unique_character("hello world")
        'h'

        >>> find_first_unique_character("aab bcdd")
        'c'

        >>> find_first_unique_character("abcdabc")
        'd'
    """

    if not isinstance(user_input, str):
        raise TypeError("'user_input' must be a string.")

    count_result = count_characters(user_input)

    # Find the first unique character
    for char in user_input:
        if char.strip() != "" and count_result[char] == 1:
            return char

    # If no unique character is found, return None
    return None


def count_character_occurrence(string_input, char_input):
    """Count the occurrence of a given character in a string.

    Args:
        string_input (str): The input string.
        char_input (str): The character to count.

    Returns:
        int: The number of times the character appears in the string.

    Examples:
        >>> count_character_occurrence("aabcdefaa", "a")
        4
    """

    if not isinstance(string_input, str) or not isinstance(char_input, str):
        raise TypeError("Both 'string_input' and 'char_input' must be strings.")

    count = 0
    for char in string_input:
        if char == char_input:
            count += 1

    return count


def convert_string_to_snake_case(input_string):
    """
    Convert a given string to snake case.

    Args:
        input_string (str): The input string to convert.

    Returns:
        str: The input string converted to snake case.

    Examples:
    >>> input_string = "This is an Example String"
    >>> snake_case_result = convert_string_to_snake_case(input_string)
    >>> print(snake_case_result)
    this_is_an_example_string

    """
    # Check if the string contains whitespace
    contains_whitespace = re.findall(r"\s+", input_string)

    if not contains_whitespace:
        # If the string does not contain whitespace, convert it to snake case
        snake_case_string = ""
        for i, char in enumerate(input_string):
            if char.isupper():
                # skip first letter of the string
                underscore = "" if i == 0 else "_"
                snake_case_string = "{}{}{}".format(snake_case_string,
                                                    underscore,
                                                    char.lower())
            elif char.isalnum() or "_" in char:
                snake_case_string += char
    else:
        # If the string contains whitespace, replace it with underscores 
        # and convert to lowercase
        snake_case_string = re.sub(r"\s+", "_", input_string)
        snake_case_string = snake_case_string.lower()

    return snake_case_string


def convert_string_to_camel_case(input_string):
    """
    Convert a given string to camel case.

    Args:
        input_string (str): The input string to convert.

    Returns:
        str: The input string converted to snake case.

    Examples:
    >>> input_string = "camel_case_name"
    >>> camel_case_result = convert_string_to_camel_case(input_string)
    >>> print(camel_case_result)
    camelCaseName

    """
    # Find all occurrences of underscore followed by a letter
    contains_underscore = re.findall(r"(_[a-z,A-Z])", input_string)

    # Find all occurrences of whitespace followed by a letter
    contains_whitespace = re.findall(r"\s+[a-z,A-Z]", input_string)

    # Convert the substrings in the input string to camel case
    def camel_case(input_string, input_list, splitter):
        """
        Convert the input string to camel case format.

        Args:
            input_string (str): The input string to be converted.
            input_list (list): List of substrings to be converted.
            splitter (str): The splitter used to split the substrings.

        Returns:
            str: The converted camel case string.

        """
        for each in input_list:
            char = each.split(splitter)
            if char:
                input_string = input_string.replace(each, char[-1].upper())
        return input_string

    # Convert underscore-separated substrings to camel case
    camel_case_string = camel_case(input_string, contains_underscore, "_")

    # Split the string by multiple delimiters (underscore and whitespace)
    word_list = re.split(' |_|-', input_string)

    # convert each word to capital except first
    capital = [each.title() for i, each in enumerate(word_list) if i!=0]
    
    camel_case_string =("".join([word_list[0].lower()] + capital))

    # Convert whitespace-separated substrings to camel case
    camel_case_string = camel_case(camel_case_string, contains_whitespace, " ")

    # Remove any remaining whitespace
    camel_case_string = camel_case_string.replace(" ", "")
    return(camel_case_string)


def reverse_string(input_string):
    """
    Reverse the given input string.

    function takes an input string and reverses it character by character. If
    the input is an integer, it is first converted to a string. The reversed
    string is built by iterating through the characters of the input string in
    reverse order and appending them to the reversed_string variable. Finally,
    the reversed string is printed and returned.

    You can call the reverse_string function with a string or an integer to
    reverse the input and obtain the reversed string.

    Args:
        input_string (str or int): The input string to be reversed. If an 
        integer is provided, it will be converted to a string.

    Returns:
        str: The reversed string.

    """

    if isinstance(input_string, int):
        input_string = str(input_string)

    reversed_string = ""
    index = len(input_string) - 1

    # Iterate through each character of the input string in reverse order
    for i, char in enumerate(input_string):
        # Append the character to the reversed string
        reversed_string += input_string[index]
        index -= 1
    return reversed_string


def replace_last_occurrence1(original_string, substring_to_replace, new_substring):
    """
    Replace the last occurrence of a substring in a string with a new substring.

    This function takes an `original_string` and replaces the last occurrence
    of substring `substring_to_replace` with the substring `new_substring`.

    Args:
        original_string (str): The original string.
        substring_to_replace (str): The substring to be replaced.
        new_substring (str): The new substring to replace `substring_to_replace`.

    Returns:
        str: A new string with the last occurrence of `substring_to_replace`
             replaced by `new_substring`.

    Example:
        original_string = "apple banana apple cherry apple"
        new_string = replace_last_occurrence1(original_string, "apple", "orange")
        print(new_string)  # Output: "apple banana apple cherry apple"
    """
    # Split the original string using the substring to replace, only once
    parts = original_string.rsplit(substring_to_replace, 1)
    
    # Join the parts using the new substring
    modified_string = new_substring.join(parts)
    
    return modified_string


def replace_last_occurrence2(original_string, substring_to_replace, new_substring):
    """
    Replace the last occurrence of a substring in a string with a new substring.

    This function takes an `original_string` and replaces the last occurrence
    of substring `substring_to_replace` with the substring `new_substring`.

    Args:
        original_string (str): The original string.
        substring_to_replace (str): The substring to be replaced.
        new_substring (str): The new substring to replace `substring_to_replace`.

    Returns:
        str: A new string with the last occurrence of `substring_to_replace`
             replaced by `new_substring`.

    Example:
        original_string = "apple banana apple cherry apple"
        new_string = replace_last_occurrence2(original_string, "apple", "orange")
        print(new_string)  # Output: "apple banana orange cherry apple"
    """
    reversed_string = original_string[::-1]
    reversed_old_substring = substring_to_replace[::-1]
    reversed_new_substring = new_substring[::-1]
    
    # Replace the last occurrence of reversed_old_substring with 
    # reversed_new_substring
    reversed_result = reversed_string.replace(reversed_old_substring,
                                              reversed_new_substring, 
                                              1)
    
    # Reverse the result to get the final modified string
    modified_string = reversed_result[::-1]
    return modified_string


def replace_last_occurrence3(original_string, substring_to_replace, new_substring):
    """
    Replace the last occurrence of a substring in a string with a new substring.

    This function takes an `original_string` and replaces the last occurrence
    of substring `substring_to_replace` with the substring `new_substring`.

    Args:
        original_string (str): The original string.
        substring_to_replace (str): The substring to be replaced.
        new_substring (str): The new substring to replace `substring_to_replace`.

    Returns:
        str: A new string with the last occurrence of `substring_to_replace`
             replaced by `new_substring`.

    Example:
        original_string = "apple banana apple cherry apple"
        new_string = replace_last_occurrence3(original_string, "apple", "orange")
        print(new_string)  # Output: "apple banana orange cherry apple"
    """
    reversed_string = reverse_string(original_string)
    reversed_old_substring = reverse_string(substring_to_replace)
    reversed_new_substring = reverse_string(new_substring)
    
    # Replace the last occurrence of reversed_old_substring with 
    # reversed_new_substring
    reversed_result = reversed_string.replace(reversed_old_substring,
                                              reversed_new_substring, 
                                              1)
    
    # Reverse the result to get the final modified string
    modified_string = reversed_result[::-1]
    return modified_string



