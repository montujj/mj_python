import unittest
from importlib import reload
import name_util as name_util
reload(name_util)


class TestUpdateSuffixNumber(unittest.TestCase):

    def setUp(self):
        self.info = 1
        self.path = r"C:\job\arrr\publish\seq17\shot129\task"
        self.increment = name_util.update_suffix_number

    def test_invalid_input_type(self):
        with self.assertRaises(ValueError):
            self.increment(123)  # Input is an integer

    def test_empty_input_string(self):
        with self.assertRaises(ValueError):
            self.increment("")  # Input is an empty string


    def test_increment_just_name_no_path_seperator(self):
        # Test incrementing version number in the existing file name
        original = "seq17_shot129_trk_cam_01.jpg"
        expected = "seq17_shot129_trk_cam_02.jpg"
        result = self.increment(original, action="increment", verbose=self.info)
        self.assertEqual(result, expected)

    def test_increment_seperator_with_one_digit_number(self):
        # Test incrementing trailing number with 1 digit padding
        original_path = rf"{self.path}\seq17_shot129_trk_cam_1.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_2.jpg"
        result = self.increment(original_path,
                                action="increment",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_increment_seperator_with_three_digit_number(self):
        # Test incrementing trailing number with 3 digit padding
        original_path = rf"{self.path}\seq17_shot129_trk_cam_001.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_002.jpg"
        result = self.increment(original_path,
                                action="increment",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_increment_no_separator(self):
        # Test incrementing trailing number without separator
        original_path = rf"{self.path}\seq17_shot129_trk_cam09.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam10.jpg"
        result = self.increment(original_path,
                                action="increment",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_increment_no_separator_no_number(self):
        # Test incrementing trailing number without separator
        original_path = rf"{self.path}\seq17_shot129_trk_cam.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_01.jpg"
        result = self.increment(original_path,
                                action="increment",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_increment_separator_no_number(self):
        # Test incrementing trailing separator without number
        original_path = rf"{self.path}\seq17_shot129_trk_cam_.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_01.jpg"
        result = self.increment(original_path,
                                action="increment",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_increment_name_underscore_number_dot_extension(self):
        # Test incrementing frame number in image sequence 
        original_path = rf"{self.path}\seq17_shot129_trk_cam_1001.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_1002.jpg"
        result = self.increment(original_path,
                                action="increment",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_increment_name_dot_number_dot_extension(self):
        # Test incrementing frame number in image sequence 
        original_path = rf"{self.path}\seq17_shot129_trk_cam05.1005.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam05.1006.jpg"
        result = self.increment(original_path,
                                action="increment",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_decrement_seperator_with_one_digit_number(self):
        #  Test decrementing trailing number with 1 digit padding
        original_path = rf"{self.path}\seq17_shot129_trk_cam_1.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_0.jpg"
        result = self.increment(original_path,
                                action="decrement",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_decrement_seperator_with_three_digit_number(self):
        #  Test incrementing trailing number with 3 digit padding
        original_path = rf"{self.path}\seq17_shot129_trk_cam_001.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_000.jpg"
        result = self.increment(original_path,
                                action="decrement",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_decrement_no_separator(self):
        # Test decrementing trailing number without separator
        original_path = rf"{self.path}\seq17_shot129_trk_cam09.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam08.jpg"
        result = self.increment(original_path,
                                action="decrement",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_decrement_no_separator_no_number(self):
        # Test decrementing trailing number without separator
        original_path = rf"{self.path}\seq17_shot129_trk_cam.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_01.jpg"
        result = self.increment(original_path,
                                action="decrement",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_decrement_separator_no_number(self):
        # Test decrementing trailing separator without number
        original_path = rf"{self.path}\seq17_shot129_trk_cam_.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_01.jpg"
        result = self.increment(original_path,
                                action="decrement",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_decrement_name_underscore_number_dot_extension(self):
        # Test decrementing frame number in image sequence 
        original_path = rf"{self.path}\seq17_shot129_trk_cam_1001.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam_1000.jpg"
        result = self.increment(original_path,
                                action="decrement",
                                verbose=self.info)
        self.assertEqual(result, expected_path)

    def test_decrement_name_dot_number_dot_extension(self):
        # Test decrementing frame number in image sequence 
        original_path = rf"{self.path}\seq17_shot129_trk_cam05.1005.jpg"
        expected_path = rf"{self.path}\seq17_shot129_trk_cam05.1004.jpg"
        result = self.increment(original_path,
                                action="decrement",
                                verbose=self.info)
        self.assertEqual(result, expected_path)


if __name__ == '__main__':
    # unittest.main()
    testloader1 = unittest.TestLoader().loadTestsFromTestCase(TestUpdateSuffixNumber)
    unittest.TextTestRunner(verbosity=2).run(testloader1)