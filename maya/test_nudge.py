import unittest
from importlib import reload

import maya.cmds as cmds
import maya.api.OpenMaya as api

import nudge as nudge
reload(nudge)


class TestNudgeNodes(unittest.TestCase):

    def setUp(self):
        # Start with a fresh Maya scene for each test
        cmds.file(new=True, force=True)

        # Create source and target nodes
        self.source_node, _ = cmds.camera(name="source")
        self.target_node =  cmds.spaceLocator(name="target")
        cmds.select(self.source_node)
        cmds.select(self.target_node, add=1)

    def test_nudge_on_camera_axis_with_positive_value(self):

        # Set initial positions of the nodes
        cmds.xform(self.source_node, translation=(0, 0, 0))
        cmds.xform(self.target_node, translation=(0, 0, -5))

        # Nudge the target node along the camera axis with a positive value
        nudge_value = 2.0
        expected_result = api.MVector(0, 0, -3)
        result = nudge.nudge_on_camera_axis(nudge_value)

        # Check if the target node has been nudged to the expected position
        self.assertAlmostEqual(result.x, expected_result.x, places=4)
        self.assertAlmostEqual(result.y, expected_result.y, places=4)
        self.assertAlmostEqual(result.z, expected_result.z, places=4)

    def test_nudge_on_camera_axis_with_negative_value(self):

        # Set initial positions of the nodes
        cmds.xform(self.source_node, translation=(0, 0, 0))
        cmds.xform(self.target_node, translation=(0, 0, -5))

        # Nudge the target node along the camera axis with a negative value
        nudge_value = -1.0
        expected_result = api.MVector(0, 0, -6)
        result = nudge.nudge_on_camera_axis(nudge_value)

        # Check if the target node has been nudged to the expected position
        self.assertAlmostEqual(result.x, expected_result.x, places=4)
        self.assertAlmostEqual(result.y, expected_result.y, places=4)
        self.assertAlmostEqual(result.z, expected_result.z, places=4)


if __name__ == '__main__':
    unittest.main(exit=False)
