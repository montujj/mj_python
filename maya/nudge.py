import maya.cmds as cmds
import maya.api.OpenMaya as api


def get_camera_from_focused_panel():
    """Get the camera associated with the currently focused panel.

    Returns:
        str: The camera name if found, None otherwise.

    """
    focused_panel = cmds.getPanel(withFocus=True)
    # Get the type of the focused panel
    panel_type = cmds.getPanel(typeOf=focused_panel)
    if panel_type == "modelPanel":
        # Get the camera associated with the model panel
        camera_shape = cmds.modelEditor(focused_panel, query=True, camera=True)
        return [camera_shape]

    return None


def get_cameras_with_image_planes():
    """
    Get a list of cameras that have associated image planes.

    This function retrieves a list of cameras that have at least one associated
    image plane in the scene.

    Returns:
        list: A list of camera names.

    Example:
        cameras_with_image_planes = get_cameras_with_image_planes()
        print(cameras_with_image_planes)
    """
    # Get a list of visible model panels
    visible_model_panels = [panel for panel in cmds.getPanel(vis=True)
                            if cmds.getPanel(typeOf=panel) == "modelPanel"]

    # Get a list of cameras associated with the visible model panels
    cameras = [cmds.modelEditor(panel, query=True, camera=True)
               for panel in visible_model_panels]

    # Initialize lists to store image plane and camera names
    image_planes = []
    cameras_with_image_planes = []

    for camera in cameras:
        # Get the history of the camera node
        history = cmds.listHistory(camera)
        
        # Filter the history to find image planes associated with the camera
        image_planes_for_camera = [node for node in history
                                   if cmds.nodeType(node) == "imagePlane"]
        
        if image_planes_for_camera:
            # If image planes are found, store the first image plane and the camera
            image_planes.append(image_planes_for_camera[0])
            cameras_with_image_planes.append(camera)

    return cameras_with_image_planes


def nudge_on_camera_axis(value=10):
    """
    Nudges the single transform node along the camera axis by user input value.

    Args:
        value (float): Nudge value.

    Returns:
        api.MVector: The resulting position of the target node.

    Examples:
    nudge_on_camera_axis(value=2)

    """
    sel = cmds.ls(selection=1, flatten=1)
    if not sel:
        raise IndexError("no selection found. Select camera, transform node")

    cam = [node for node in sel
           for shp in cmds.ls(node, dagObjects=1, shapes=1)
           if cmds.nodeType(shp) == "camera"]
    if cam:
        sel.remove(cam[0])
    else:
        cam = get_camera_from_focused_panel()
        if not cam:
            cam = get_cameras_with_image_planes()

    if not cam:
        raise TypeError("no camera found in selection or focused panel")

    source_node = cam[0]
    target_node = sel[0]

    # Get the world space translation vectors of the nodes
    query_params = dict(query=True, worldSpace=True, translation=True)
    vec2 = api.MVector(*cmds.xform(target_node, **query_params))
    vec1 = api.MVector(*cmds.xform(source_node, **query_params))

    # Calculate the distance between the nodes
    vec3 = vec1 - vec2
    distance = vec3.length()

    # Calculate the nudged position
    u_value = value / distance
    nudged_pos = vec2 + (u_value * vec3)

    # Move the target node to the nudged position
    cmds.xform(target_node, worldSpace=True, translation=(nudged_pos.x,
                                                          nudged_pos.y,
                                                          nudged_pos.z))

    # Select the source and target nodes
    cmds.select(source_node)
    cmds.select(target_node, add=True)

    return nudged_pos
