

def flatten(nested_array):
    """Flatten multiple level of nested arrays into new single array recursively.

    Args:
        nested_array (list): arbitrarily nested arrays of integers

    Returns:
        list: flattened list with all items at top level

    Examples:
        >>> flatten([[1, 2, [3]], 4])
        [1, 2, 3, 4]
        
        >>> flatten([[61, 25, [36]], [5, 6, 7 ,[9, [15]]]])
        [61, 25, 36, 5, 6, 7, 9, 15]
    """
    
    if not isinstance(nested_array, list):
        raise TypeError('Input must be type "list"')
    
    result = []

    def inner_flatten(nested_array):

        # helper function to flatten multiple level of nested arrays 
        # recursively. if integer found update variable "result"
        # at top level, else recursively extract each items from 
        # nested lists
    
        for each in nested_array:
            if isinstance(each, list): 
                inner_flatten(each) 
            else:
                result.append(each) 

    # flattened nested list here using helper function and return result
    inner_flatten(nested_array)
    return result 
