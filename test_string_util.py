import unittest
from importlib import reload
import string_util as string_util
reload(string_util)

class TestFindMaximumOccurrenceCharacter(unittest.TestCase):

    def setUp(self):
        self.param = dict(verbose=0)

    def test_max_occurrence_single_character(self):
        result = string_util.find_maximum_occurrence_character("a", 
                                                               **self.param)
        self.assertEqual(result, "a")

    def test_max_occurrence_multiple_characters_with_maximum_occurrence(self):
        result = string_util.find_maximum_occurrence_character("hello",
                                                               **self.param)
        self.assertEqual(result, "l")

    def test_max_occurrence_multiple_characters_with_equal_occurrence(self):
        result = string_util.find_maximum_occurrence_character("abbccc",
                                                               **self.param)
        self.assertEqual(result, "c")

    def test_max_occurrence_empty_string(self):
        result = string_util.find_maximum_occurrence_character("",
                                                               **self.param)
        self.assertIsNone(result)

    def test_max_occurrence_string_with_spaces(self):
        result = string_util.find_maximum_occurrence_character("hello world",
                                                               **self.param)
        self.assertEqual(result, "l")

    def test_max_occurrence_string_with_special_characters(self):
        result = string_util.find_maximum_occurrence_character("@#$@@@!!", 
                                                               **self.param)
        self.assertEqual(result, "@")


class TestFindFirstUniqueCharacter(unittest.TestCase):

    def test_unique_character(self):
        # Test with a non-repeated character
        result = string_util.find_first_unique_character("hello")
        self.assertEqual(result, 'h')

    def test_unique_character_with_spaces(self):
        # Test with a non-repeated character and spaces
        result = string_util.find_first_unique_character("hello world")
        self.assertEqual(result, 'h')

    def test_non_unique_characters(self):
        # Test with all characters repeated
        result = string_util.find_first_unique_character("aabbccdd")
        self.assertIsNone(result)

    def test_unique_empty_string(self):
        # Test with an empty string
        result = string_util.find_first_unique_character("")
        self.assertIsNone(result)

    def test_unique_only_spaces(self):
        # Test with only spaces
        result = string_util.find_first_unique_character("       ")
        self.assertIsNone(result)

    def test_unique_user_input_not_string(self):
        with self.assertRaises(TypeError):
            string_util.find_first_unique_character([])

    # def test_value(self):
    #     self.assertRaises(TypeError, string_util.find_first_unique_character, [])


class TestCountCharacterOccurrence(unittest.TestCase):

    def test_count_occurrence(self):
        # Test with a string containing multiple occurrences of the character
        result = string_util.count_character_occurrence("aabcdefaa", "a")
        self.assertEqual(result, 4)

    def test_count_occurrence_empty_string(self):
        # Test with an empty string
        result = string_util.count_character_occurrence("", "a")
        self.assertEqual(result, 0)

    def test_count_occurrence_empty_char(self):
        # Test with an empty character
        result = string_util.count_character_occurrence("aabcdefaa", "")
        self.assertEqual(result, 0)

    def test_count_occurrence_no_occurrence(self):
        # Test with a character that does not occur in the string
        result = string_util.count_character_occurrence("aabcdefaa", "z")
        self.assertEqual(result, 0)

    def test_count_occurrence_string_not_string(self):
        # Test with string_input not being a string
        with self.assertRaises(TypeError):
            string_util.count_character_occurrence(123, "a")

    def test_count_occurrence_char_not_string(self):
        # Test with char_input not being a string
        with self.assertRaises(TypeError):
            string_util.count_character_occurrence("aabcdefaa", 123)


class TestConvertStringToSnakeCase(unittest.TestCase):

    def setUp(self):
        self.expected_output = "this_is_an_example_string"

    def test_snake_case_no_whitespace(self):
        input_string = "ThisIsAnExampleString"
        result = string_util.convert_string_to_snake_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_snake_case_with_whitespace(self):
        input_string = "This is an Example String"
        result = string_util.convert_string_to_snake_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_snake_case_only_whitespace(self):
        input_string = "      "
        expected_output = "_"
        result = string_util.convert_string_to_snake_case(input_string)
        self.assertEqual(result, expected_output)

    def test_snake_case_special_characters(self):
        input_string = "@#example!String"
        expected_output = "example_string"
        result = string_util.convert_string_to_snake_case(input_string)
        self.assertEqual(result, expected_output)

    def test_snake_case_all_uppercase(self):
        input_string = "THIS IS AN EXAMPLE STRING"
        result = string_util.convert_string_to_snake_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_snake_case_already_snake_case(self):
        input_string = "this_is_an_example_string"
        result = string_util.convert_string_to_snake_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_snake_case_no_input(self):
        input_string = ""
        result = string_util.convert_string_to_snake_case(input_string)
        self.assertEqual(result, "")


class TestConvertStringToCamelCase(unittest.TestCase):

    def setUp(self):
        self.expected_output = "camelCaseName"

    def test_camel_case_no_whitespace_with_underscore(self):
        input_string = "camel_case_name"
        result = string_util.convert_string_to_camel_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_camel_case_with_whitespace_no_underscore(self):
        input_string = "camel case name"
        result = string_util.convert_string_to_camel_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_camel_case_with_whitespace_and_underscore(self):
        input_string = "camel_case name"
        result = string_util.convert_string_to_camel_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_camel_case_with_underscore_upper_whitespace(self):
        input_string = "camel_Case Name"
        result = string_util.convert_string_to_camel_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_camel_case_no_underscore_upper_whitespace(self):
        input_string = "camel Case Name"
        result = string_util.convert_string_to_camel_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_camel_case_no_upper_whitespace(self):
        input_string = "CAMEL Case Name"
        result = string_util.convert_string_to_camel_case(input_string)
        self.assertEqual(result, self.expected_output)

    def test_camel_case_no_input(self):
        input_string = ""
        result = string_util.convert_string_to_camel_case(input_string)
        self.assertEqual(result, "")


class TestReverseString(unittest.TestCase):

    def test_reverse_number_string(self):
        input_string = "12345"
        self.expected_output = "54321"
        result = string_util.reverse_string(input_string)
        self.assertEqual(result, self.expected_output)

    def test_reverse_integers(self):
        input_string = 12345
        self.expected_output = "54321"
        result = string_util.reverse_string(input_string)
        self.assertEqual(result, self.expected_output)

    def test_reverse_alpha_string(self):
        input_string = "test string"
        self.expected_output = "gnirts tset"
        result = string_util.reverse_string(input_string)
        self.assertEqual(result, self.expected_output)

    def test_reverse_empty(self):
        input_string = ""
        self.expected_output = ""
        result = string_util.reverse_string(input_string)
        self.assertEqual(result, self.expected_output)

    def test_reverse_space(self):
        input_string = " "
        self.expected_output = " "
        result = string_util.reverse_string(input_string)
        self.assertEqual(result, self.expected_output)


class TestReplaceLastOccurrence1Rsplit(unittest.TestCase):

    def setUp(self):
        self.replace1 = string_util.replace_last_occurrence1

    def test_replace_last_occurrence_rsplit_with_single_occurrence(self):
        original_string = "london paris france germany paris"
        new_string = self.replace1(original_string, "paris", "france")
        expected_output = "london paris france germany france"
        self.assertEqual(new_string, expected_output)

    def test_replace_last_occurrence_rsplit_with_multiple_occurrences(self):
        original_string = "hello world hello"
        new_string = self.replace1(original_string, "hello", "hi")
        expected_output = "hello world hi"
        self.assertEqual(new_string, expected_output)

    def test_replace_last_occurrence_rsplit_no_occurrence(self):
        original_string = "this is a test"
        new_string = self.replace1(original_string, "missing", "replacement")
        self.assertEqual(new_string, original_string)

    def test_replace_last_occurrence_rsplit_empty_string(self):
        original_string = ""
        new_string = self.replace1(original_string, "substring", "new_substring")
        self.assertEqual(new_string, "")

    def test_replace_last_occurrence_rsplit_with_whitespace(self):
        original_string = "this is ateststring"
        new_string = self.replace1(original_string, "test", " replacement ")
        expected_output = "this is a replacement string"
        self.assertEqual(new_string, expected_output)


class TestReplaceLastOccurrence2Reverse(unittest.TestCase):

    def setUp(self):
        self.replace2 = string_util.replace_last_occurrence2

    def test_replace_last_occurrence2_reverse_with_single_occurrence(self):
        original_string = "london paris france germany paris"
        new_string = self.replace2(original_string, "paris", "france")
        expected_output = "london paris france germany france"
        self.assertEqual(new_string, expected_output)

    def test_replace_last_occurrence2_reverse_with_multiple_occurrences(self):
        original_string = "hello world hello"
        new_string = self.replace2(original_string, "hello", "hi")
        expected_output = "hello world hi"
        self.assertEqual(new_string, expected_output)

    def test_replace_last_occurrence2_reverse_no_occurrence(self):
        original_string = "this is a test"
        new_string = self.replace2(original_string, "missing", "replacement")
        self.assertEqual(new_string, original_string)

    def test_replace_last_occurrence2_reverse_empty_string(self):
        original_string = ""
        new_string = self.replace2(original_string, "substring", "new_substring")
        self.assertEqual(new_string, "")

    def test_replace_last_occurrence2_reverse_with_whitespace(self):
        original_string = "this is ateststring"
        new_string = self.replace2(original_string, "test", " replacement ")
        expected_output = "this is a replacement string"
        self.assertEqual(new_string, expected_output)


class TestReplaceLastOccurrence3Reverse(unittest.TestCase):

    def setUp(self):
        self.replace3 = string_util.replace_last_occurrence3

    def test_replace_last_occurrence3_reverse_with_single_occurrence(self):
        original_string = "london paris france germany paris"
        new_string = self.replace3(original_string, "paris", "france")
        expected_output = "london paris france germany france"
        self.assertEqual(new_string, expected_output)

    def test_replace_last_occurrence3_reverse_with_multiple_occurrences(self):
        original_string = "hello world hello"
        new_string = self.replace3(original_string, "hello", "hi")
        expected_output = "hello world hi"
        self.assertEqual(new_string, expected_output)

    def test_replace_last_occurrence3_reverse_no_occurrence(self):
        original_string = "this is a test"
        new_string = self.replace3(original_string, "missing", "replacement")
        self.assertEqual(new_string, original_string)

    def test_replace_last_occurrence3_reverse_empty_string(self):
        original_string = ""
        new_string = self.replace3(original_string, "substring", "new_substring")
        self.assertEqual(new_string, "")

    def test_replace_last_occurrence3_reverse_with_whitespace(self):
        original_string = "this is ateststring"
        new_string = self.replace3(original_string, "test", " replacement ")
        expected_output = "this is a replacement string"
        self.assertEqual(new_string, expected_output)

if __name__ == '__main__':

    test1 = unittest.TestLoader().loadTestsFromTestCase(TestFindMaximumOccurrenceCharacter)
    unittest.TextTestRunner(verbosity=2).run(test1)

    test2 = unittest.TestLoader().loadTestsFromTestCase(TestFindFirstUniqueCharacter)
    unittest.TextTestRunner(verbosity=2).run(test2)

    test3 = unittest.TestLoader().loadTestsFromTestCase(TestCountCharacterOccurrence)
    unittest.TextTestRunner(verbosity=2).run(test3)

    test4 = unittest.TestLoader().loadTestsFromTestCase(TestConvertStringToSnakeCase)
    unittest.TextTestRunner(verbosity=2).run(test4)

    test5 = unittest.TestLoader().loadTestsFromTestCase(TestConvertStringToCamelCase)
    unittest.TextTestRunner(verbosity=2).run(test5)

    test6 = unittest.TestLoader().loadTestsFromTestCase(TestReverseString)
    unittest.TextTestRunner(verbosity=2).run(test6)

    test7 = unittest.TestLoader().loadTestsFromTestCase(TestReplaceLastOccurrence1Rsplit)
    unittest.TextTestRunner(verbosity=2).run(test7)

    test8 = unittest.TestLoader().loadTestsFromTestCase(TestReplaceLastOccurrence2Reverse)
    unittest.TextTestRunner(verbosity=2).run(test8)

    test9 = unittest.TestLoader().loadTestsFromTestCase(TestReplaceLastOccurrence3Reverse)
    unittest.TextTestRunner(verbosity=2).run(test9)