import os
import re
import getpass
import logging
from importlib import reload

import path_util as path_util
reload(path_util)

root, name_ext = os.path.split(__file__)
name, ext = os.path.splitext(name_ext)
log_file = f"{name}.log"
# log_file = 'name_util.log'

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
username = getpass.getuser()

# Create a formatter with a custom date format & user name
formatter = logging.Formatter('%(levelname)s - '
                              '%(lineno)d - '
                              '%(name)s - '
                              '%(asctime)s - '
                              # '%(message)s', datefmt='%b %d, %Y %H:%M:%S %p')
                              '%(message)s', datefmt='%Y-%m-%d (%I:%M:%S %p) - '
                              f'{username}')

# Create the file handler, set level to ERROR and set the formatter
file_handler = logging.FileHandler(log_file)
file_handler.setLevel(logging.ERROR)
file_handler.setFormatter(formatter)

# Create the stream handler and set the formatter
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

# Add the file handler and stream handler to the logger
logger.addHandler(file_handler)
logger.addHandler(stream_handler)


def update_suffix_number(file_path, action='increment', verbose=1):
    """
    Generate new path by incrementing or decrementing the trailing number

    Args:
        file_path (str): The original file path.
        action (str, optional): The action to perform - 'increment' or 
                                'decrement'. Defaults to 'increment'.
        verbose (int, optional): Controls whether to print verbose output. 
                                 Defaults to 1.

    Returns:
        str: The modified file path.

    Examples:
        This function can be used to generate incremented or decremented paths 
        for files with trailing numbers.

        For example, given the following names:

        names = ["job_seq_shot_plates_jpg_v001.1001.jpg",
                 "job_seq_shot_plates_jpg_v001_1.jpg",
                 "job_seq_shot_plates_jpg_v001.1.jpg",  
                 "job_seq_shot_plates_jpg_v001.01.jpg",
                 "job_seq_shot_plates_jpg_v001.001.jpg", 
                 "job_seq_shot_plates_jpg_v001.12.jpg",
                 "job_seq_shot_plates_jpg_v001.472.jpg",
                 "job_seq_shot_plates_jpg_v001.1234566.jpg",
                 "job_seq_shot_plates_jpg_v001.0.jpg",
                 "track_file_001.jpg",
                 "track_file_v001.jpg",  
                 "track_file.jpg",
                 "track_file01.jpg", 
                 "track_file_.jpg",
                 "track234_file_.jpg",
                 "track756_file.jpg",
                 "track234_file_3487.jpg"
                 ]

        for name in names:
            root = r"Z:\\job\\arrr\\pub\\seq\\shot\\plates\\"
            file_path = f"{root}arrr_seq_shot_task_plates_jpg_v001\\{name}"
            file_path =  name
            update_suffix_number(file_path=file_path, action='increment')

        # update_suffix_number(file_path="", action='increment')
        # update_suffix_number(file_path=None, action='increment')

    """
    # check if input is not string raise ValueError
    if not isinstance(file_path, str) or file_path == "":
        error_message  = ">> user input must be a string\n"
        logger.error(error_message)
        raise ValueError(error_message)

    # adjust path separators & ensure path is correctly formatted for current OS
    file_path = path_util.normalize_path_separators(file_path)

    if verbose:
        logger.info(f"  {action}: input : {file_path}")

    char = "_"

    # Split the original path into directory and file name with extension
    directory, name_ext = os.path.split(file_path)

    # Split the file name and extension
    name, ext = os.path.splitext(name_ext)

    # Extract the number from the file name (if it exists)

    pattern = r"\d+$"
    number_match = re.search(pattern, name)

    if number_match:
        # Extract the number portion of the version
        number = number_match.group()

        padding = (len(number.split(char)[-1])) if char else 2

        # Increment or decrement the number based on the specified action
        new_number = int(number) + (1 if action == 'increment' else -1)

        # new_name = name.replace(number, f"{new_number:0{padding}d}")

        # Split name at last occurence of the number. create the new name by 
        # formatting the new number with appropriate padding and joining it 
        # with the first part of the name
        new_name = f"{new_number:0{padding}d}".join(name.rsplit(number, 1))

    else:
        new_name = f"{name}{char}01" if not name.endswith(char) else f"{name}01"   

    # Generate the new path by joining the directory and new file name
    new_path = os.path.join(directory, new_name)

    # Append the original file extension to the new path
    new_path_ext = f"{new_path}{ext}"

    if verbose:
        # Print the new path with extension
        logger.info(f"  {action}: output: {new_path_ext}\n")
    return new_path_ext
